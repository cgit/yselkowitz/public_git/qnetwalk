Name:           qnetwalk
Version:        1.6
Release:        1%{?dist}
Summary:        Network administration game
License:        GPLv2
URL:            https://github.com/AMDmi3/%{name}/
Source0:        https://github.com/AMDmi3/%{name}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  qt5-qttools-devel
BuildRequires:  SDL_mixer-devel

%description
QNetWalk is a game for system administrators.

%prep
%setup -q

%build
%cmake
%cmake_build

%install
%cmake_install

%find_lang %{name} --with-qt --without-mo

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop || :

%files -f %{name}.lang
%doc README.md
%license COPYING
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/sounds/
%dir %{_datadir}/%{name}/translations
%{_datadir}/pixmaps/%{name}.xpm
%{_mandir}/man6/%{name}.6.*


%changelog
* Tue Nov 30 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 1.6-1
- new version

* Sun Aug  2 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 1.5-1
- Initial package
